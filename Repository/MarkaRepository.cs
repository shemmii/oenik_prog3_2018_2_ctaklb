﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
  public  class MarkaRepository:IRepository<Marka>
    {
     private readonly  Model1 data = new Model1();
            
        public IQueryable<Marka> ListAll()
        {
            return this.data.Marka.AsQueryable();
            //foreach (var item in data.Marka)
            //{
            //    string Marka =item.ID + " " + item.NEV + " " + item.ORSZAG + " " + item.EV;
            //    Console.WriteLine(Marka);
            //}
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="entity">entity</param>
        public void Insert(Marka entity)
        {
            data.Marka.Add(entity);
            data.SaveChanges();
             Console.WriteLine("Új elem beszúrva!");
        }
        /// <summary>
        /// remove
        /// </summary>
        /// <param name="nev">name</param>
        public void Remove(int nev)
        {
            Marka torlendo = data.Marka.Single(x => x.ID == nev);
            data.Marka.Remove(torlendo);
            data.SaveChanges();
            Console.WriteLine("Törlés megtörtént");

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="oszlop">column</param>
        public void Update(int id,int oszlop)
        {
            var elem = this.data.Marka.Single(x => x.ID.Equals( id));
            switch (oszlop)
            {
                case 1:
                    Console.WriteLine("Új Márkanév: ");
                    elem.NEV= Console.ReadLine();
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 2:
                    Console.WriteLine("Új Márka ország: ");
                    elem.ORSZAG = Console.ReadLine();
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 3:
                    Console.WriteLine("Új Márka alapítás év: ");
                    elem.EV = Convert.ToInt32(Console.ReadLine());
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
            }
        }
    }
}
