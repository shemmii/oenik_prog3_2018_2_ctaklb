﻿using System;
using Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ModellReposiroty : IRepository<Modell>
    {
        private readonly Model1 data = new Model1();

        public IQueryable<Modell> ListAll()
        {
            return this.data.Modell.AsQueryable();
            //foreach (var item in data.Modell)
            //{
            //    //string Modell = item.ID + " " + item.NEV + " " + item.EV + " " + item.MOTORTERFOGAT + " " + item.LOERO;
            //    //Console.WriteLine(Modell);
            //}
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="entity">entity</param>
        public void Insert(Modell entity)
        {
            data.Modell.Add(entity);
            data.SaveChanges();
            Console.WriteLine("Új elem beszúrva!");

        }
        /// <summary>
        /// remove
        /// </summary>
        /// <param name="nev">name</param>
        public void Remove(int nev)
        {
            Modell torlendo = data.Modell.Single(x => x.ID == nev);
            data.Modell.Remove(torlendo);
            data.SaveChanges();
            Console.WriteLine("Törlés megtörtént");

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="oszlop">column</param>
        public void Update(int id, int oszlop)
        {
            var elem = this.data.Modell.Single(x => x.ID.Equals(id));
            switch (oszlop)
            {
                case 1:
                    Console.WriteLine("Új Modellnév: ");
                    elem.NEV = Console.ReadLine();
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 2:
                    Console.WriteLine("Új Modell gyártási év: ");
                    elem.EV = Convert.ToInt32(Console.ReadLine());
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 3:
                    Console.WriteLine("Új Modell motortérfogat: ");
                    elem.MOTORTERFOGAT = Convert.ToInt32(Console.ReadLine());
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 4:
                    Console.WriteLine("Új Modell lóerő: ");
                    elem.LOERO = Convert.ToInt32(Console.ReadLine());
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
            }

        }
    }
}
