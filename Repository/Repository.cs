﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> ListAll();
        void Insert(TEntity entity);
        void Remove(int entity);
        void Update(int id, int oszlop);
        
    }



}
