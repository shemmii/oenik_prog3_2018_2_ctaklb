﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ExtraRepository : IRepository<Extra>
    {
        private readonly Model1 data = new Model1();

        public IQueryable<Extra> ListAll()
        {
            return this.data.Extra.AsQueryable();
            //foreach (var item in data.Extra)
            //{
            //    string Extra =item.ID + " " + item.NEV + " " + item.AR + " " + item.SZIN;
            //    Console.WriteLine(Extra);
            //}
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="entity">entity</param>
        public void Insert(Extra entity)
        {

            data.Extra.Add(entity);
            data.SaveChanges();
            Console.WriteLine("Új elem beszúrva!");

        }
        /// <summary>
        /// remove
        /// </summary>
        /// <param name="nev">name</param>
        public void Remove(int nev)
        {
            Extra torlendo = data.Extra.Single(x => x.ID == nev);
            data.Extra.Remove(torlendo);
            data.SaveChanges();
            Console.WriteLine("Törlés megtörtént");

        }
        /// <summary>
        /// update
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="oszlop">column</param>
        public void Update(int id, int oszlop)
        {
            var elem = this.data.Extra.Single(x => x.ID.Equals(id));
            switch (oszlop)
            {
                case 1:
                    Console.WriteLine("Új Extra neve: ");
                    elem.NEV = Console.ReadLine();
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 2:
                    Console.WriteLine("Új Extra ár: ");
                    elem.AR =Convert.ToInt32( Console.ReadLine());
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
                case 3:
                    Console.WriteLine("Új Extra szín: ");
                    elem.SZIN = Console.ReadLine();
                    this.data.SaveChanges();
                    Console.WriteLine("Változtatások megtörténtek!");
                    break;
            }

        }
    }
}
