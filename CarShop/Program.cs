﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Logic;

    using Repository;

    /// <summary>
    /// program
    /// </summary>
   internal class Program
    {
        private static readonly MarkaRepository Markarepo = new MarkaRepository();
        private static readonly ModellReposiroty Modellrepo = new ModellReposiroty();
        private static readonly ExtraRepository Extrarepo = new ExtraRepository();

        private static void Main()
        {
            Menu();
            Console.Read();
        }

        private static void Menu()
        {
            ILogic logic = new AppLogic(Markarepo, Modellrepo, Extrarepo);
            bool kilep = false;
            while (!kilep)
            {
                Console.WriteLine("MENÜ");
                Console.WriteLine("1 - Listázás");
                Console.WriteLine("2 - Új elem beszúrása    ");
                Console.WriteLine("3 - Elem törlése");
                Console.WriteLine("4 - Elem módosítása");
                Console.WriteLine("***összetett adatok***");
                Console.WriteLine("5 - Évenként kiadott Modellek száma");
                Console.WriteLine("6 - Extrák színenkénti átlagára");
                Console.WriteLine("7 - Megadott országból származó márkák");
                int menu = Convert.ToInt16(Console.ReadLine());
                if (menu == 0)
                {
                    kilep = true;
                }

                Console.Clear();
                switch (menu)
                {
                    case 0:
                        {
                            System.Environment.Exit(1);
                            break;
                        }

                    case 1:
                        {
                            Listaz();
                            break;
                        }

                    case 2:
                        {
                            Beszur();
                            break;
                        }

                    case 3:
                        {
                            Torol();
                            break;
                        }

                    case 4:
                        {
                            Modosit();
                            break;
                        }

                    case 5:
                        {
                          Console.WriteLine(logic.ModellEvben());
                            break;
                        }

                    case 6:
                        {
                            Console.WriteLine(logic.SzinAtlagar());
                            break;
                        }

                    case 7:
                        {
                            Console.WriteLine("Adja meg az országot: ");
                            string orszag = Console.ReadLine();
                            Console.WriteLine(logic.OrszagMarka(orszag));
                            break;
                        }
                }
            }
        }

        private static void Listaz()
        {
            Console.Clear();
             ILogic logic = new AppLogic(Markarepo, Modellrepo, Extrarepo);
            Console.WriteLine("1 - Márkák");
            Console.WriteLine("2 - Modellek");
            Console.WriteLine("3 - Extrák");

            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    var marka = (IQueryable<Data.Marka>)logic.ListMarka();
                    marka.ToList();
                    foreach (var item in marka)
                    {
                        string elem = item.ID + " " + item.NEV + " " + item.ORSZAG + " " + item.EV;
                        Console.WriteLine(elem);
                    }

                    break;
                case 2:
                    var modell = (IQueryable<Data.Modell>)logic.ListModell();
                    modell.ToList();
                    foreach (var item in modell)
                    {
                        string elem = item.ID + " " + item.NEV + " " + item.EV + " " + item.MOTORTERFOGAT + " " + item.LOERO;
                        Console.WriteLine(elem);
                    }

                    break;
                case 3:
                    var extra = (IQueryable<Data.Extra>)logic.ListExtra();
                    extra.ToList();
                    foreach (var item in extra)
                    {
                        string elem = item.ID + " " + item.NEV + " " + item.AR + " " + item.SZIN;
                        Console.WriteLine(elem);
                    }

                    break;
                default:
                    break;
            }
        }

        private static void Beszur()
        {
            Console.Clear();
            ILogic logic = new AppLogic(Markarepo, Modellrepo, Extrarepo);
            Console.WriteLine("Mit szeretne hozzáadni?");
            Console.WriteLine();
            Console.WriteLine("1 - Márka");
            Console.WriteLine("2 - Modell");
            Console.WriteLine("3 - Extra");
            int m = Convert.ToInt16(Console.ReadLine());
            switch (m)
            {
                case 1:
                    Console.WriteLine("ID: ");
                    int marka_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("NEV: ");
                    string marka_nev = Console.ReadLine();
                    Console.WriteLine("ORSZAG: ");
                    string marka_orszag = Console.ReadLine();
                    Console.WriteLine("EV: ");
                    int marka_ev = Convert.ToInt32(Console.ReadLine());

                    logic.AddMarka(marka_id, marka_nev, marka_orszag, marka_ev);
                    break;
                case 2:
                    Console.WriteLine("ID: ");
                    int modell_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Marka_ID: ");
                    int modell_Marka_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("NEV: ");
                    string modell_nev = Console.ReadLine();
                    Console.WriteLine("EV: ");
                    int modell_ev = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("MOTORTERFOGAT: ");
                    int modell_motor = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("LOERO: ");
                    int modell_loero = Convert.ToInt32(Console.ReadLine());
                    logic.AddModell(modell_id, modell_Marka_id, modell_nev, modell_ev, modell_motor, modell_loero);
                    break;
                case 3:
                    Console.WriteLine("ID: ");
                    int extra_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Modell_ID: ");
                    int extra_Modell_id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("NEV: ");
                    string extra_nev = Console.ReadLine();
                    Console.WriteLine("AR: ");
                    int extra_ar = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("SZIN: ");
                    string extra_szin = Console.ReadLine();
                    logic.AddExtra(extra_id, extra_Modell_id, extra_nev, extra_ar, extra_szin);
                    break;
                default:
                    break;
            }
        }

        private static void Torol()
        {
            Console.Clear();
            ILogic logic = new AppLogic(Markarepo, Modellrepo, Extrarepo);
            Console.WriteLine("Mit szeretne törölni?");
            Console.WriteLine();
            Console.WriteLine("1 - Márka");
            Console.WriteLine("2 - Modell");
            Console.WriteLine("3 - Extra");
            int m = Convert.ToInt16(Console.ReadLine());
            int elem;
            Console.Write("Törlendő elem ID-ja: ");

            switch (m)
            {
                case 1:
                    elem = Convert.ToInt32(Console.ReadLine());
                    logic.RemoveMarka(elem);
                    break;
                case 2:
                    elem = Convert.ToInt32(Console.ReadLine());
                    logic.RemoveModell(elem);
                    break;
                case 3:
                    elem = Convert.ToInt32(Console.ReadLine());
                    logic.RemoveExtra(elem);
                    break;
            }
}

        private static void Modosit()
        {
            Console.Clear();
            ILogic logic = new AppLogic(Markarepo, Modellrepo, Extrarepo);
            Console.WriteLine("Mit szeretne módosítani?");
            Console.WriteLine();
            Console.WriteLine("1 - Márka");
            Console.WriteLine("2 - Modell");
            Console.WriteLine("3 - Extra");
            int m = Convert.ToInt16(Console.ReadLine());
            switch (m)
            {
                case 1:
                    Console.WriteLine("Módosítandó márka id-ja: ");
                    int markaid = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Módosítandó mező: ");
                    Console.WriteLine("1 - Név");
                    Console.WriteLine("2 - Ország");
                    Console.WriteLine("3 - Alapítás éve");
                    int markaoszlop = Convert.ToInt32(Console.ReadLine());
                    logic.UpdateMarka(markaid, markaoszlop);
                    break;
                case 2:
                    Console.WriteLine("Módosítandó Modell id-ja: ");
                    int modellid = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Módosítandó mező: ");
                    Console.WriteLine("1 - Név");
                    Console.WriteLine("2 - Év");
                    Console.WriteLine("3 - Motor térfogat");
                    Console.WriteLine("4 - lóerő");
                    int modelloszlop = Convert.ToInt32(Console.ReadLine());
                    logic.UpdateModell(modellid, modelloszlop);
                    break;
                case 3:
                    Console.WriteLine("Módosítandó Extra id-ja: ");
                    int extraid = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Módosítandó mező: ");
                    Console.WriteLine("1 - Név");
                    Console.WriteLine("2 - ár");
                    Console.WriteLine("3 - szín");
                    int extraoszlop = Convert.ToInt32(Console.ReadLine());
                    logic.UpdateExtra(extraid, extraoszlop);
                    break;
            }
        }
    }
}
