﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Repository;
using Data;
using Logic;


namespace Logic.Tests
{

   [TestFixture]
     class Tests
    {


        [Test]
        public void Test_EgyMarkaHozzaadas()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            List<Data.Marka> Markak = new List<Data.Marka>()
            {
                new Data.Marka() {ID=22, NEV="Tesztnev", ORSZAG="Tesztország", EV = 2018}
            };
            MarkaMockRepo.Setup(repo => repo.ListAll()).Returns(Markak.AsQueryable());

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
            var result = (IQueryable<Data.Marka>)logic.ListMarka();

            // ASSERT
            Assert.That(result.Count(), Is.EqualTo(1));

        }
        [Test]
        public void Test_EgyModellHozzaadas()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            List<Data.Modell> Modellek = new List<Data.Modell>()
            {
                new Data.Modell() {ID=9999, Marka_ID=10, NEV="Modellnev",
                EV=2018, MOTORTERFOGAT=9999,LOERO=9999}
            };
            ModellMockRepo.Setup(repo => repo.ListAll()).Returns(Modellek.AsQueryable());

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
            var result = (IQueryable<Data.Modell>)logic.ListModell();

            // ASSERT
            Assert.That(result.Count(), Is.EqualTo(1));
        }
        [Test]
        public void Test_EgyExtraHozzaadas()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            List<Data.Extra> Extrak = new List<Data.Extra>()
            {
                new Data.Extra() {ID=9999,Modell_ID=9999,NEV="Extranev",AR=9999,SZIN="Extraszin"}
            };
           ExtraMockRepo.Setup(repo => repo.ListAll()).Returns(Extrak.AsQueryable());

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
            var result = (IQueryable<Data.Extra>)logic.ListExtra();

            // ASSERT
            Assert.That(result.Count(), Is.EqualTo(1));
        }
        [Test]
        public void Test_NincsujMarka()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();
            List<Data.Marka> Markak = new List<Data.Marka>()
            {
                new Data.Marka() {ID=22, NEV="Tesztnev", ORSZAG="Tesztország", EV = 2018}
            };
            MarkaMockRepo.Setup(repo => repo.ListAll()).Returns(Markak.AsQueryable());

        //    AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
            

            // ASSERT
            MarkaMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Marka>()), Times.Never);
        }
        [Test]
        public void Test_nincsujModell()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            List<Data.Modell> Modellek = new List<Data.Modell>()
            {
                new Data.Modell() {ID=9999, Marka_ID=10, NEV="Modellnev",
                EV=2018, MOTORTERFOGAT=9999,LOERO=9999}
            };
            ModellMockRepo.Setup(repo => repo.ListAll()).Returns(Modellek.AsQueryable());

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
         //   var result = (IQueryable<Data.Modell>)logic.ListModell();

            // ASSERT
           ModellMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Modell>()), Times.Never);
        }
        [Test]
        public void Test_nincsujExtra()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            List<Data.Extra> Extrak = new List<Data.Extra>()
            {
                new Data.Extra() {ID=9999,Modell_ID=9999,NEV="Extranev",AR=9999,SZIN="Extraszin"}
            };
            ExtraMockRepo.Setup(repo => repo.ListAll()).Returns(Extrak.AsQueryable());

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);
            // ACT
         //   var result = (IQueryable<Extra>)logic.ListExtra();

            // ASSERT
            ExtraMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Extra>()), Times.Never);
        }
        [Test]
        public void Test_Markaegyszerfutle()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);

            // ACT
            logic.AddMarka(22, "tesztMarka", "tesztorszag", 2018);

            // ASSERT
           MarkaMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Marka>()), Times.Once);
        }
        [Test]
        public void Test_Modellegyszerfutle()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);

            // ACT
            logic.AddModell(9999, 9999, "tesztModell", 2018, 9999, 9999);

            // ASSERT
            ModellMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Modell>()), Times.Once);
        }

        [Test]
        public void Test_Markaupdate()
        {
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);

            // ACT
            logic.UpdateModell(11, 1);

            // ASSERT
            MarkaMockRepo.Verify(repo => repo.Update(11, 1), Times.Never);
        }

        [Test]
        public void Test_Extraegyszerfutle()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);

            // ACT
            logic.AddExtra(9999, 9999, "tesztExtra", 9999, "tesztszín");

            // ASSERT
            ExtraMockRepo.Verify(repo => repo.Insert(It.IsAny<Data.Extra>()), Times.Once);
        }
        [Test]
        public void Test_Modelltorlesidalapjan()
        {
            // ARRANGE
            Mock<IRepository<Data.Marka>> MarkaMockRepo = new Mock<IRepository<Data.Marka>>();
            Mock<IRepository<Data.Modell>> ModellMockRepo = new Mock<IRepository<Data.Modell>>();
            Mock<IRepository<Data.Extra>> ExtraMockRepo = new Mock<IRepository<Data.Extra>>();

            AppLogic logic = new AppLogic(MarkaMockRepo.Object, ModellMockRepo.Object, ExtraMockRepo.Object);

            // ACT
            logic.RemoveModell(9999);

            // ASSERT
            ModellMockRepo.Verify(repo => repo.Remove(9999));


        }

       
    }
}
