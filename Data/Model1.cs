// <auto-generated/>
namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<Marka> Marka { get; set; }
        public virtual DbSet<Modell> Modell { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Extra>()
                .Property(e => e.ID)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Extra>()
                .Property(e => e.Modell_ID)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Extra>()
                .Property(e => e.NEV)
                .IsUnicode(false);

            modelBuilder.Entity<Extra>()
                .Property(e => e.AR)
                .HasPrecision(10, 0);

            modelBuilder.Entity<Extra>()
                .Property(e => e.SZIN)
                .IsUnicode(false);

            modelBuilder.Entity<Marka>()
                .Property(e => e.ID)
                .HasPrecision(2, 0);

            modelBuilder.Entity<Marka>()
                .Property(e => e.NEV)
                .IsUnicode(false);

            modelBuilder.Entity<Marka>()
                .Property(e => e.ORSZAG)
                .IsUnicode(false);

            modelBuilder.Entity<Marka>()
                .Property(e => e.EV)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Marka>()
                .HasMany(e => e.Modell)
                .WithRequired(e => e.Marka)
                .HasForeignKey(e => e.Marka_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Modell>()
                .Property(e => e.ID)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Modell>()
                .Property(e => e.Marka_ID)
                .HasPrecision(2, 0);

            modelBuilder.Entity<Modell>()
                .Property(e => e.NEV)
                .IsUnicode(false);

            modelBuilder.Entity<Modell>()
                .Property(e => e.EV)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Modell>()
                .Property(e => e.MOTORTERFOGAT)
                .HasPrecision(6, 0);

            modelBuilder.Entity<Modell>()
                .Property(e => e.LOERO)
                .HasPrecision(4, 0);

            modelBuilder.Entity<Modell>()
                .HasMany(e => e.Extra)
                .WithRequired(e => e.Modell)
                .HasForeignKey(e => e.Modell_ID)
                .WillCascadeOnDelete(false);
        }
    }
}
