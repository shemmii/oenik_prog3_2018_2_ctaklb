// <auto-generated/>
namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Modell")]
    public partial class Modell
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Modell()
        {
            Extra = new HashSet<Extra>();
        }

        [Column(TypeName = "numeric")]
        public decimal ID { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Marka_ID { get; set; }

        [StringLength(10)]
        public string NEV { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? EV { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MOTORTERFOGAT { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? LOERO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Extra> Extra { get; set; }

        public virtual Marka Marka { get; set; }
    }
}
