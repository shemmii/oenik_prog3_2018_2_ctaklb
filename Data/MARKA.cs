// <auto-generated/>
namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Marka")]
    public partial class Marka
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Marka()
        {
            Modell = new HashSet<Modell>();
        }

        [Column(TypeName = "numeric")]
        public decimal ID { get; set; }

        [StringLength(14)]
        public string NEV { get; set; }

        [StringLength(13)]
        public string ORSZAG { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? EV { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Modell> Modell { get; set; }
    }
}
