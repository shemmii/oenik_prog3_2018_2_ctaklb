﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    /// <summary>
    /// ILogic interface
    /// </summary>
   public interface ILogic
    {
        /// <summary>
        /// marks
        /// </summary>
        /// <returns>marks</returns>
        IQueryable ListMarka();
        /// <summary>
        /// modells
        /// </summary>
        /// <returns>modells</returns>
        IQueryable ListModell();
        /// <summary>
        /// Extras
        /// </summary>
        /// <returns>Extras</returns>
        IQueryable ListExtra();
        /// <summary>
        /// Add new Marka
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="nev">nev</param>
        /// <param name="orszag">orszag</param>
        /// <param name="ev">ev</param>
        void AddMarka(int id, string nev, string orszag, int ev);
        /// <summary>
        /// add new model
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="Marka_id">Markaid</param>
        /// <param name="nev">name</param>
        /// <param name="ev">year</param>
        /// <param name="motorterfogat">engine</param>
        /// <param name="loero">horsepower</param>
        void AddModell(int id, int Marka_id, string nev, int ev, int motorterfogat, int loero);
        /// <summary>
        /// add new Extra
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="Modell_id">modellid</param>
        /// <param name="nev">name</param>
        /// <param name="ar">price</param>
        /// <param name="szin">color</param>
        void AddExtra(int id, int Modell_id, string nev, int ar, string szin);
        /// <summary>
        /// remove mark
        /// </summary>
        /// <param name="nev">name</param>
        void RemoveMarka(int nev);
        /// <summary>
        /// remove modell
        /// </summary>
        /// <param name="nev">name</param>
        void RemoveModell(int nev);
        /// <summary>
        /// remove Extra
        /// </summary>
        /// <param name="nev">name</param>
        void RemoveExtra(int nev);
        /// <summary>
        /// update mark
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        void UpdateMarka(int id, int mezo);
        /// <summary>
        /// update modell
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        void UpdateModell(int id, int mezo);
        /// <summary>
        /// update Extra
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        void UpdateExtra(int id, int mezo);
        /// <summary>
        /// return modells in a year
        /// </summary>
        /// <returns>modells and years</returns>
        string ModellEvben();
        /// <summary>
        /// return average color prices
        /// </summary>
        /// <returns>colors and prices</returns>
        string SzinAtlagar();
        /// <summary>
        /// return the marks in the country
        /// </summary>
        /// <param name="orszag">country</param>
        /// <returns>marks details</returns>
        string OrszagMarka(string orszag);

    }
}
