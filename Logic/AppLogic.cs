﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository;


namespace Logic
{
    /// <summary>
    /// applogic
    /// </summary>
    public class AppLogic:ILogic
    {
        private readonly IRepository<Marka> MarkaRepo;
        private readonly IRepository<Modell> ModellRepo;
        private readonly IRepository<Extra> ExtraRepo;
        /// <summary>
        /// appligic
        /// </summary>
        /// <param name="MarkaRepo">mark</param>
        /// <param name="modell">modell</param>
        /// <param name="ExtraRepo">Extra</param>
        public AppLogic(
             IRepository<Marka> MarkaRepo,
         IRepository<Modell> modell,
         IRepository<Extra> ExtraRepo)
        {
            this.MarkaRepo = MarkaRepo;
            this.ModellRepo = modell;
            this.ExtraRepo = ExtraRepo;
        }
        /// <summary>
        /// lst of marks
        /// </summary>
        /// <returns>marks</returns>
        public IQueryable ListMarka()
        {
            var list = from item in this.MarkaRepo.ListAll()
                    select item;
            return list;
        }
        /// <summary>
        /// lost of modells
        /// </summary>
        /// <returns>modells</returns>
        public IQueryable ListModell()
        {
            var list = from item in this.ModellRepo.ListAll()
                       select item;
            return list;
        }
        /// <summary>
        /// list of Extras
        /// </summary>
        /// <returns>exttras</returns>
        public IQueryable ListExtra()
        {
            var list = from item in this.ExtraRepo.ListAll()
                       select item;
            return list;
        }
        /// <summary>
        /// add new mark
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="nev">name</param>
        /// <param name="orszag">country</param>
        /// <param name="ev">year</param>
        public void AddMarka(int id,string nev, string orszag, int ev)
        {
            this.MarkaRepo.Insert(
                new Marka()
                {
                    ID =id,
                    NEV = nev,
                    ORSZAG = orszag,
                    EV = ev
                   });
        }
        /// <summary>
        /// add new model
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="Marka_id">Markaid</param>
        /// <param name="nev">name</param>
        /// <param name="ev">year</param>
        /// <param name="motorterfogat">engine</param>
        /// <param name="loero">horsepower</param>
        public void AddModell(int id, int Marka_id, string nev, int ev, int motorterfogat, int loero)
        {
            this.ModellRepo.Insert(
                new Modell()
                {
                    ID = id,
                    Marka_ID = Marka_id,
                    NEV = nev,
                    EV = ev,
                    MOTORTERFOGAT = motorterfogat,
                    LOERO = loero
                });
        }
        /// <summary>
        /// add new Extra
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="Modell_id">modellid</param>
        /// <param name="nev">name</param>
        /// <param name="ar">price</param>
        /// <param name="szin">color</param>
        public void AddExtra(int id,int Modell_id,string nev,int ar,string szin)
        {
            this.ExtraRepo.Insert(
                new Extra()
                {
                    ID = id,
                    Modell_ID = Modell_id,
                    NEV = nev,
                    AR = ar,
                    SZIN = szin
                });
        }
        /// <summary>
        /// remove mark
        /// </summary>
        /// <param name="nev">name</param>
        public void RemoveMarka(int nev)
        {
            this.MarkaRepo.Remove(nev);
        }
        /// <summary>
        /// remove modell
        /// </summary>
        /// <param name="nev">name</param>
        public void RemoveModell(int nev)
        {
            this.ModellRepo.Remove(nev);
        }
        /// <summary>
        /// remove Extra
        /// </summary>
        /// <param name="nev">name</param>
        public void RemoveExtra(int nev)
        {
           this.ExtraRepo.Remove(nev);
        }

        /// <summary>
        /// update mark
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        public void UpdateMarka(int id, int mezo)
        {
            this.MarkaRepo.Update(id, mezo);
        }
        /// <summary>
        /// update modell
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        public void UpdateModell(int id, int mezo)
        {
            this.ModellRepo.Update(id, mezo);
        }
        /// <summary>
        /// update Extra
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="mezo">column</param>
        public void UpdateExtra(int id, int mezo)
        {
            this.ExtraRepo.Update(id, mezo);
        }
        /// <summary>
        /// modell in years
        /// </summary>
        /// <returns>modells and years</returns>
        public string ModellEvben()
        {
            string vissza = string.Empty;

            // var Modellek = this.ModellRepo.ListAll();

            var szurtModellek = from m in this.ModellRepo.ListAll()
                                group m by m.EV into g
                                orderby g.Count() descending
                                select new
                                {
                                    EV = g.Key,
                                    COUNT = g.Count()
                                          
                                      };

            foreach (var item in szurtModellek)
            {
                vissza +=" év: " + item.EV + "        Darab: " + item.COUNT + "\n";
            }

            return vissza;

            
        }
        /// <summary>
        /// color average price
        /// </summary>
        /// <returns>colors and prices</returns>
        public string SzinAtlagar()
        {
            string vissza = string.Empty;
            var average = from m in this.ExtraRepo.ListAll()
                          group m by m.SZIN into g
                          select new
                          {
                              ATLAG = g.Average(x => x.AR),
                              SZIN = g.Key
                          };
            foreach (var item in average)
            {
                vissza += "Szín: " + item.SZIN + "    Átlagár: " + item.ATLAG + "\n";
            }

            return vissza;
        }
        /// <summary>
        /// country and mark
        /// </summary>
        /// <param name="orszag">country</param>
        /// <returns></returns>
        public string OrszagMarka(string orszag)
        {
            string vissza = string.Empty;

            var osszes = this.MarkaRepo.ListAll();

            var szurtMarka = from m in osszes
                                      where m.ORSZAG.Contains(orszag)
                                      select new
                                      {
                                         ID=m.ID,
                                          NEV=m.NEV,
                                          EV=m.EV
                                      };

            foreach (var item in szurtMarka)
            {
                vissza += " ID: " + item.ID + "     Márkanév: " +item.NEV + "      Alapítás éve: " + item.EV + "\n";
            }

            return vissza;
        }
    }
}
